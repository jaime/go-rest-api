package api

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/jaime/go-rest-api/internal/handler"
	"gitlab.com/jaime/go-rest-api/internal/schema"
	"gitlab.com/jaime/go-rest-api/internal/store"
)

type storage interface {
	CreatePet(pet *schema.Pet) error
	GetPet(id int) (*schema.Pet, error)
	ListPets() (schema.Pets, error)
}

// API defines the handlers to be used by our router.
// It holds a store and any other dependencies
type API struct {
	store storage
}

// New creates an API with a store
func New(store storage) *API {
	return &API{
		store: store,
	}
}

// CreatePet handler that saves the pet to store
func (a *API) CreatePet(w http.ResponseWriter, r *http.Request) {
	var pet schema.Pet

	if err := json.NewDecoder(r.Body).Decode(&pet); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		// nolint
		w.Write([]byte(`{"error":"` + err.Error() + `"}`))

		return
	}

	if err := a.store.CreatePet(&pet); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		// nolint
		w.Write([]byte(`{"error":"` + err.Error() + `"}`))

		return
	}

	b, err := json.Marshal(pet)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		// nolint
		w.Write([]byte(`{"error":"` + err.Error() + `"}`))
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	// nolint
	w.Write(b)
}

// GetPet finds a pet by ID
func (a *API) GetPet(r *http.Request) (interface{}, error) {
	values := r.URL.Query()

	idStr := values["id"]
	if len(idStr) != 1 {
		return nil, handler.NewError(http.StatusBadRequest, "missing query string: id")
	}

	id, err := strconv.Atoi(idStr[0])
	if err != nil {
		return nil, handler.NewError(http.StatusBadRequest, "bad query string: id is not a number")
	}

	p, err := a.store.GetPet(id)
	if err != nil {
		if err == store.ErrPetNotFound {
			return nil, handler.NewError(http.StatusNotFound, err.Error())
		}
	}

	return p, nil
}

// ListPets returns a list of all pets
func (a *API) ListPets(_ *http.Request) (interface{}, error) {
	return a.store.ListPets()
}
