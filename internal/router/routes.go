package router

import "net/http"

// Route defines the path, method and handler for a route
type Route struct {
	Path    string
	Method  string
	Handler http.HandlerFunc
}

// NewRoute creates a new route
func NewRoute(path, method string, handler http.HandlerFunc) *Route {
	return &Route{
		Path:    path,
		Method:  method,
		Handler: handler,
	}
}
