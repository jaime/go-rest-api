package std

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/jaime/go-rest-api/internal/router"
)

// New creates an http.ServerMux initialising routes in handlers
func New(routes []*router.Route) *http.ServeMux {
	mux := http.NewServeMux()

	for _, route := range routes {
		mux.HandleFunc(route.Path, route.Handler)
	}

	mux.HandleFunc("/hello/", hello)

	return mux
}

func hello(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}

	params := strings.Split(r.URL.Path, "/")
	msg := struct {
		Message string
	}{
		Message: fmt.Sprintf("Hello from stdlib, %s!", params[2]),
	}

	//nolint
	json.NewEncoder(w).Encode(msg)
}
