package gorilla

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/jaime/go-rest-api/internal/router"
)

// New creates a gorilla mux router
func New(routes []*router.Route) *mux.Router {
	r := mux.NewRouter()

	for _, route := range routes {
		r.Path(route.Path).Methods(route.Method).HandlerFunc(route.Handler)
	}

	r.Path("/hello/{name}").Methods(http.MethodGet).HandlerFunc(hello)

	return r
}

func hello(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	msg := struct {
		Message string
	}{
		Message: fmt.Sprintf("Hello from gorilla, %s!", params["name"]),
	}

	//nolint
	json.NewEncoder(w).Encode(msg)
}
