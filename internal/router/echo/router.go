package echo

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"gitlab.com/jaime/go-rest-api/internal/middleware"
	"gitlab.com/jaime/go-rest-api/internal/router"
)

// New ..
func New(routes []*router.Route, log *logrus.Entry) *echo.Echo {
	e := echo.New()
	logger := middleware.RequestLogger(log)
	e.Use(echo.WrapMiddleware(logger))

	for _, route := range routes {
		route := route

		switch route.Method {
		case "GET":
			e.GET(route.Path, echo.WrapHandler(route.Handler))
		case "POST":
			e.POST(route.Path, echo.WrapHandler(route.Handler))
		}
	}

	e.GET("/hello/:name", hello)

	return e
}

func hello(c echo.Context) error {
	msg := struct {
		Message string `json:"message"`
	}{
		Message: fmt.Sprintf("Hello from echo, %s!", c.Param("name")),
	}

	return c.JSON(http.StatusOK, msg)
}
