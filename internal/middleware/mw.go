package middleware

import (
	"net/http"
	"runtime/debug"
	"time"

	"github.com/sirupsen/logrus"
)

var reqID int

const accessTokenHeader = "x-access-token"

// RequestLogger ...
func RequestLogger(log *logrus.Entry) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()

			l := log.WithFields(logrus.Fields{
				"req_id": reqID,
				"method": r.Method,
				"path":   r.URL.Path,
			})

			defer func(l *logrus.Entry) {
				if r := recover(); r != nil {
					l.Warnf("recovered from panic: %+v", r)
					l.Error(string(debug.Stack()))
				}
			}(l)

			reqID++

			l.Info("request...")

			next.ServeHTTP(w, r)

			l = l.WithFields(logrus.Fields{
				"duration": time.Since(start),
			})
			l.Info("response...")
		}

		return http.HandlerFunc(fn)
	}
}

// AccessToken checks if token exists or not
func AccessToken(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		accessToken := r.Header.Get(accessTokenHeader)
		if accessToken != "123456" {
			http.Error(w, "unauthorized access", http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
