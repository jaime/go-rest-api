package store

import (
	"errors"
	"sync"

	"gitlab.com/jaime/go-rest-api/internal/schema"
)

// ErrPetNotFound ...
var ErrPetNotFound = errors.New("pet not found")

// Memory naive implementation of an in-memory store
type Memory struct {
	mu   *sync.Mutex
	Pets map[int]*schema.Pet
}

// New ..
func New() *Memory {
	return &Memory{
		mu:   &sync.Mutex{},
		Pets: make(map[int]*schema.Pet)}
}

// GetPet ..
func (m *Memory) GetPet(id int) (*schema.Pet, error) {
	m.mu.Lock()
	defer m.mu.Unlock()

	p, ok := m.Pets[id]
	if !ok {
		return nil, ErrPetNotFound
	}

	return p, nil
}

// CreatePet ..
func (m *Memory) CreatePet(pet *schema.Pet) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	m.Pets[pet.ID] = pet

	return nil
}

// ListPets ..
func (m *Memory) ListPets() (schema.Pets, error) {
	m.mu.Lock()
	defer m.mu.Unlock()

	pets := make(schema.Pets, len(m.Pets))

	i := 0

	for _, p := range m.Pets {
		pets[i] = p
		i++
	}

	return pets, nil
}
