package schema

// PetType ..
type PetType string

const (
	// Dog ..
	Dog PetType = "dog"

	// Cat ..
	Cat PetType = "cat"
)

// Pet describes a type of pet
type Pet struct {
	ID   int     `json:"id"`
	Name string  `json:"name"`
	Type PetType `json:"type"`
}

// Pets contains a list of Pet
type Pets []*Pet
