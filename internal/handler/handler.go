package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// Error implements the error interface
type Error struct {
	StatusCode int    `json:"status_code"`
	Message    string `json:"message"`
}

// Error converts the Error to a string representation
func (e *Error) Error() string {
	return fmt.Sprintf("status: %d message: %q", e.StatusCode, e.Message)
}

// NewError creates a handler.Error with status code and message
func NewError(code int, message string) *Error {
	return &Error{
		StatusCode: code,
		Message:    message,
	}
}

// Func is a custom wrapper around http.HandlerFunc. It allow handlers to return
// a custom response or an error
func Func(f func(r *http.Request) (interface{}, error)) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		res, err := f(r)
		if err != nil {
			writeError(w, err)
			return
		}

		writeJSON(w, http.StatusOK, res)
	}

	return fn
}

func writeError(w http.ResponseWriter, err error) {
	if err == nil {
		return
	}

	httpErr, ok := err.(*Error)
	if !ok {
		writeError(w, NewError(http.StatusInternalServerError, err.Error()))
		return
	}

	writeJSON(w, httpErr.StatusCode, httpErr)
}

func writeJSON(w http.ResponseWriter, code int, res interface{}) {
	b, err := json.Marshal(res)
	if err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json")
	// nolint hope for the best
	w.Write(b)
}
