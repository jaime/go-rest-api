module gitlab.com/jaime/go-rest-api

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/labstack/echo/v4 v4.1.16
	github.com/sirupsen/logrus v1.5.0
)
