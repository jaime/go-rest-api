PROJECT ?= go-rest-api

.PHONY: mod build run test

mod:
	go mod tidy
	go mod vendor

build: mod
	rm -rf ./bin/*
	go build  -o ./bin/$(PROJECT) ./cmd/$(PROJECT)/

run: build
	./bin/$(PROJECT)


test:
	go test ./... -count 1 -timeout 30s -v


test-race:
	go test ./... -count 1 -timeout 30s -race

cover:
	@echo "NOTE: make cover does not exit 1 on failure, don't use it to check for tests success!"
	rm -f cover/*.out
	go test ./... -coverprofile=cover/unit.out
	go tool cover -html cover/unit.out -o cover/coverage.html
	@echo ""
	@echo "=====> Total test coverage: <====="
	@echo ""
	go tool cover -func cover/unit.out

lint:
	docker run --rm -v $(PWD):/go/$(PROJECT) -w /go/$(PROJECT) registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine golangci-lint run
