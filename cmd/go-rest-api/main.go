package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/jaime/go-rest-api/internal/api"
	"gitlab.com/jaime/go-rest-api/internal/handler"
	"gitlab.com/jaime/go-rest-api/internal/middleware"
	"gitlab.com/jaime/go-rest-api/internal/router"
	"gitlab.com/jaime/go-rest-api/internal/router/echo"
	"gitlab.com/jaime/go-rest-api/internal/router/gorilla"
	"gitlab.com/jaime/go-rest-api/internal/router/std"
	"gitlab.com/jaime/go-rest-api/internal/store"
)

// TODO this main is ugly, maybe I should DRY it a bit
func main() {
	a := api.New(store.New())
	routes := []*router.Route{
		router.NewRoute("/pets", "GET", handler.Func(a.ListPets)),
		router.NewRoute("/pet", "POST", a.CreatePet),
		router.NewRoute("/get-pet", "GET", handler.Func(a.GetPet)),
	}

	stdLog := logrus.NewEntry(logrus.New()).WithField("server", "stdlib")
	logger := middleware.RequestLogger(stdLog)
	stdMux := std.New(routes)

	stdSrv := &http.Server{
		Addr:    ":8081",
		Handler: logger(stdMux),
	}

	go func() {
		logrus.Info("stdlib server listening on :8081...")

		if err := stdSrv.ListenAndServe(); err != nil {
			logrus.WithError(err).Error("failed to start stdlib server")
		}
	}()

	gorillaLog := logrus.NewEntry(logrus.New()).WithField("server", "gorilla")

	gorillaMux := gorilla.New(routes)
	gorillaMux.Use(middleware.RequestLogger(gorillaLog))
	gorillaMux.Use(middleware.AccessToken)

	gorillaSrv := &http.Server{
		Addr:    ":8082",
		Handler: gorillaMux,
	}

	go func() {
		logrus.Info("gorilla server listening on :8082...")

		if err := gorillaSrv.ListenAndServe(); err != nil {
			logrus.WithError(err).Error("failed to start gorilla server")
		}
	}()

	echoLog := logrus.NewEntry(logrus.New()).WithField("server", "echo")
	echoSrv := echo.New(routes, echoLog)

	go func() {
		logrus.Info("echo server listening on :8083...")

		if err := echoSrv.Start(":8083"); err != nil {
			logrus.WithError(err).Error("failed to start echo server")
		}
	}()

	handleExitSignals([]*http.Server{
		stdSrv,
		gorillaSrv,
		echoSrv.Server,
	})
}

func handleExitSignals(servers []*http.Server) {
	exitCh := make(chan os.Signal, 1)
	signal.Notify(exitCh, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP)
	<-exitCh

	logrus.Info("gracefully stopping servers")

	wg := sync.WaitGroup{}

	for _, srv := range servers {
		wg.Add(1)

		go func(wg *sync.WaitGroup, srv *http.Server) {
			defer wg.Done()

			ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
			defer cancel()

			if err := srv.Shutdown(ctx); err != nil {
				logrus.WithError(err).Error("failed to shutdown server")
			}
		}(&wg, srv)
	}

	wg.Wait()
	logrus.Info("servers stopped")
}
