# Go REST API

Sample REST API written in Go. 
It uses 2 different frameworks and the standard library `http.ServeMux`
to serve requests through three different servers and their routers.

- [http.ServeMux](https://golang.org/pkg/net/http/#NewServeMux) on port `:8081`
- [Gorilla Mux](https://github.com/gorilla/mux) on port `:8082`
- [echo](https://github.com/labstack/echo) on port `:8083`

The aim of this simple project is to demonstrate how you can write a 
handler that can be used with any router. It is also an attempt
to show the differences between some of the more ~opinionated~ specific
ways of writing handlers for different frameworks. 

I"ve also added an attempt at structuring a Go project here. I've mostly been
inspired by [Golang Standards' Project Layour](https://github.com/golang-standards/project-layout).
I would also recommed looking at [Ardan Labs' Ultimate Service](https://github.com/ardanlabs/service).

Nowadays, there's so [many different HTTP frameworks](https://www.mindinventory.com/blog/top-web-frameworks-for-development-golang/) 
and each will surely have its own advantages and disadvantages. 
However, I decided to make the comparison with the frameworks I've used the most.
Maybe some day I will try some others out there 😉.
 
## Run

> Requires Go1.13 or higher.

Builds a binary under `./bin/go-rest-api` and runs it

```shell
make run

# output
./bin/go-rest-api
INFO[0000] stdlib server listening on :8081...
INFO[0000] gorilla server listening on :8082...
INFO[0000] echo server listening on :8083...

   ____    __
  / __/___/ /  ___
 / _// __/ _ \/ _ \
/___/\__/_//_/\___/ v4.1.16
High performance, minimalist Go web framework
https://echo.labstack.com
____________________________________O/_______
                                    O\
⇨ http server started on [::]:8083
```

## Test

Run unit tests

```shell
make test
```

With race detector

```shell
make test-race
```
## Lint

Runs [golangci-lint](https://github.com/golangci/golangci-lint) on all files. 
See [.golangci.yml](.golangci.yml) for details.

```shell
make lint
```


## TODOs

- [ ] Document everything better
- [ ] Add unit tests
- [ ] Write a client to talk to these services
- [ ] Add integration tests 
- [ ] Add a DB store 🤔
- [ ] Make services configurable, maybe try [viper](https://github.com/spf13/viper)

## Questions?

Reach out on [@jimbart098](https://twitter.com/jimbart098) or [LinkedIn](https://www.linkedin.com/in/jaime-m88/)
